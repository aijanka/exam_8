import React, {Component, Fragment} from 'react';
import {MenuItem, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {NavLink, Route, Switch} from "react-router-dom";
import AllQuotesList from "./containers/AllQuotesList/AllQuotesList";
import AddQuote from "./containers/AddQuote/AddQuote";
import EditQuote from "./containers/EditQuote/EditQuote";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navbar>
                    <Navbar.Header><Navbar.Brand><NavLink to='/'>Quotes Central</NavLink></Navbar.Brand></Navbar.Header>
                    <Nav  pullRight>
                        <li><NavLink className='' to='/'>Quotes</NavLink></li>
                        <li><NavLink to='/add-quote'>Submit new quote</NavLink></li>
                        <NavDropdown title="Categories" id="basic-nav-dropdown">
                            <li><NavLink to='/quotes'>All quotes</NavLink></li>
                            <MenuItem divider />
                            <li><NavLink to='/quotes/typicalVK' exact>Typical VK</NavLink></li>
                            <li><NavLink to='/quotes/famous-people' exact>Famous people</NavLink></li>
                            <li><NavLink to='/quotes/humour' exact>Humour</NavLink></li>
                            <li><NavLink to='/quotes/motivational' exact>Motivational</NavLink></li>
                        </NavDropdown>
                    </Nav>
                </Navbar>

                <Switch>
                    <Route path='/' exact render={(props) => (<AllQuotesList category={'all'} {...props}/>)}/>
                    <Route path='/quotes' exact render={(props) => (<AllQuotesList category={'all'} {...props}/>)}/>
                    <Route path='/add-quote' exact component={AddQuote}/>

                    <Route path='/quotes/:id/edit'  exact render={(props => (<EditQuote {...props}/>))}/>

                    <Route path='/quotes/typicalVK' exact render={(props) => (<AllQuotesList category={'typicalVK'} {...props}/>)}/>
                    <Route path='/quotes/famous-people' exact render={(props) => (<AllQuotesList category={'famous-people'} {...props}/>)}/>
                    <Route path='/quotes/humour' exact render={(props) => (<AllQuotesList category={'humour'} {...props}/>)}/>
                    <Route path='/quotes/motivational' exact render={(props) => (<AllQuotesList category={'motivational'} {...props}/>)}/>

                </Switch>
            </Fragment>
        );
    }
}

export default App;
