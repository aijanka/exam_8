import React, {Component} from 'react';
import axios from 'axios';
import {Grid} from "react-bootstrap";
import Quote from "../../components/Quote/Quote";

class AllQuotesList extends Component {
    state = {
        quotes: null,
        category: null
    };

    componentDidUpdate() {
        if ((this.state.quotes && this.state.category !== this.props.category)) {
            if (this.props.category === 'all') {
                this.getAllQuotes();
                this.setState({category: this.props.category});
            } else {

                axios.get(`/quotes.json?orderBy="category"&equalTo="${this.props.category}"`).then(response => {
                    const quotes = response.data;
                    this.setState({quotes, category: this.props.category});
                });
            }
        }
    }

    componentDidMount() {
        this.getAllQuotes();
    }

    getAllQuotes() {
        axios.get('/quotes.json').then(response => {
            const quotes = response.data;
            this.setState({quotes});
        });
    }

    deleteQuote = id => {
        axios.delete(`quotes/${id}.json`).then(() => {
            this.setState(() => {
                this.getAllQuotes()
            })
        })
    };

    render() {
        const quotes = this.state.quotes;
        if (quotes) {
            const ids = Object.keys(this.state.quotes);

            return (
                <Grid>
                    {ids.map(quoteId => (<Quote
                                            key={quoteId}
                                            id={quoteId}
                                            author={quotes[quoteId].author}
                                            category={quotes[quoteId].category}
                                            text={quotes[quoteId].text}
                                            // edit={() => this.editQuote(quoteId)}
                                            delete={() => this.deleteQuote(quoteId)}
                                        />))
                    }
                </Grid>
            )
        } else {
            return <h1>No quotes yet (</h1>
        }

    }
}

export default AllQuotesList;