import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Grid} from "react-bootstrap";
import axios from 'axios';

class EditQuote extends Component {
    state = {
        quote: {
            category: this.props.category,
            author: this.props.author,
            text: this.props.text
        },
        loading: false

    };

    componentDidMount () {
        axios.get('/quotes.json').then(response => {
            const quote = response.data[this.props.match.params.id];
            this.setState({quote});
        })
    }

    saveChange = event => {
        const quote = {...this.state.quote};
        quote[event.target.name] = event.target.value;
        this.setState({quote});
    };

    editQuote = (id, event) =>  {
        event.preventDefault();
        // this.setState({loading: true});

        axios.patch(`/quotes/${id}.json`, this.state.quote);
        this.props.history.replace('/');
    };

    render () {
        return (
            <Grid>
                <form>
                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Quote Author</ControlLabel>
                        <FormControl
                            componentClass="textarea"
                            placeholder="textarea"
                            name='author'
                            value={this.state.quote.author}
                            onChange={this.saveChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Category</ControlLabel>
                        <FormControl
                            componentClass="select"
                            placeholder="Select category"
                            name ='category'
                            onChange={this.saveChange}
                            value={this.state.quote.category}
                        >
                            <option value="typicalVK">Typical VK</option>
                            <option value="famous-people">Famous people</option>
                            <option value="humour">Humour</option>
                            <option value="motivational">Motivational</option>
                        </FormControl>
                    </FormGroup>


                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Quote Text</ControlLabel>
                        <FormControl
                            componentClass="textarea"
                            placeholder="textarea"
                            name='text'
                            value={this.state.quote.text}
                            onChange={this.saveChange}
                        />

                    </FormGroup>

                    <Button bsStyle="primary" onClick={(event) => this.editQuote(this.props.match.params.id, event)}>Submit</Button>
                </form>
            </Grid>

        )
    }
}

export default EditQuote;