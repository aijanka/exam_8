import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Grid} from "react-bootstrap";
import axios from 'axios';

class AddQuote extends Component {
    state = {
        quote: {
            category: 'typicalVK',
            author: '',
            text: ''
        },
        loading: false,
    };


    saveChange = event => {
        const quote = this.state.quote;
        quote[event.target.name] = event.target.value;
        this.setState({quote});
    };

    addQuote = event => {
        event.preventDefault();

        this.setState({loading: true});

        axios.post('/quotes.json', this.state.quote).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/');
        })


    }

    render() {

        return (
            <Grid>
                <form>
                    <ControlLabel>Author</ControlLabel>
                    <FormControl
                        type="text"
                        name='author'
                        placeholder="Enter author of the quote"
                        onChange={this.saveChange}
                    />

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Category</ControlLabel>
                        <FormControl componentClass="select" placeholder="Select category" name='category'
                                     onChange={this.saveChange}>
                            <option value="typicalVK">Typical VK</option>
                            <option value="famous-people">Famous people</option>
                            <option value="humour">Humour</option>
                            <option value="motivational">Motivational</option>
                        </FormControl>
                    </FormGroup>


                    <FormGroup controlId="formControlsTextarea">
                        <ControlLabel>Quote Text</ControlLabel>
                        <FormControl
                            componentClass="textarea"
                            placeholder="textarea"
                            name='text'
                            onChange={this.saveChange}
                        />
                    </FormGroup>


                    <Button type="submit" onClick={this.addQuote}>Submit</Button>
                </form>

            </Grid>

        )
    }
}

export default AddQuote;