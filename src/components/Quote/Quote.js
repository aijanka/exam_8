import React from 'react';
import {Grid, Jumbotron} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import FontAwesome from 'react-fontawesome';
import './Quote.css';

const Quote = props => {
    const link = `/quotes/${props.id}/edit`;
    return (
        <Grid>
            <Jumbotron className='QuoteContainer'>
                <h3>{props.author}</h3>
                <p>{props.text}</p>
                <p className='QuoteBtns'>
                    <NavLink to={link}>
                        <FontAwesome
                            className='fas fa-edit'
                            name='edit'
                            size='2x'
                            style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
                        />
                    </NavLink>
                    <a onClick={props.delete}>
                        <FontAwesome
                            className='fas fa-trash-alt'
                            name='trash'
                            size='2x'
                            style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
                        />
                    </a>
                </p>
            </Jumbotron>
        </Grid>

    )
};

export default Quote;